#### 19 juillet 2022 - Emilie Caillault

#### ----------------------------------------------------------
#### Global environment - Memory Cleaning. 
#### ----------------------------------------------------------
rm(list=ls(all=TRUE))
graphics.off()

#### -----------------------------------------------------------------------
#### 0-Loading functions, Packages 
#### ----------------------------------------------------------------------*-
#
require("rio")
require("ggplot2")
require("zoo")

#directory of this file (only for source) 
# for Run current line specify the script.dir
script.dir="D:/1Recherche/0MAREL"
if(!dir.exists(script.dir)){
  script.dir <- dirname(sys.frame(1)$ofile)
  print(script.dir)
}

# 0- CHANGE your source directory (R function)
sourcesDirectory=file.path(script.dir,'codeR')

if (!is.null(sourcesDirectory)){
  files.R=list.files(path = sourcesDirectory, full.names=T, ignore.case = T,pattern = (".R"))
  #remove script files
  indScript=grep("script",files.R)
  if (length(indScript)){files.R=files.R[-indScript]}
  print(files.R)
  for (f in files.R) source(f,local=T)
  rm(f)
  rm(files.R)
  rm(indScript)
}


#0- CHANGE your save directory and nameFile (name of your dataset for instance)
nameFile="CARNOT"
saveDirectory=file.path(script.dir,nameFile,'save31aout2022')
if (!dir.exists(saveDirectory)) dir.create(saveDirectory);
setwd(saveDirectory) 


#### ----------------------------------------------------------------------------------------------------------------------------------------------------------------
#### 0- TODO: specify File name
# dataFile : name of the raw Coriolis File
# infoFile : Sensor range, eoV, toKeep, aggregateFunction
#### ----------------------------------------------------------------------------------------------------------------------------------------------------------------
dataFile=file.path(script.dir,"Carnot-6200443.csv")
sensorsFile=file.path(script.dir,"SensorsRange.csv")

resDir=file.path(saveDirectory,'Rdata')
resFIG=file.path(saveDirectory,"FIG")
if (!dir.exists(resDir)){
  dir.create(resDir);
  dir.create(resFIG,"FIG");
}
#### ----------------------------------------------------------------------------------------------------------------------------------------------------------------
# 0- TODO: change T or F for actions

# 1- importation and apply QC
applyImport=F
applyQCviewer=F; 
#2-remove some data with QC
QCtoremove=NULL; #add values with QC to remove: QCtoremove=c(0;4) for instance
#3- remove data out of sensorrange
applyMetrology=F
#4- Time Alignment
timeAlignment=F

#### ----------------------------------------------------------------------------------------------------------------------------------------------------------------
#### 1- importation and QC application
# one QC vector by line that corresponds to all column before QC

# '0' No QC is performed
# '1' Good data. All Argo real-time QC tests passed. 
# '2' Probably good data. Probably good data. to be used with caution.
# '3' Probably bad data that are potentially adjustable. 
# '4' Bad data Bad data. These measurements are not to be used. e.g. due to sensor failure.
# '5' Value changed Value changed Value changed
# '6' Not used Not used Not used
# '7' Not used Not used Not used
# '8' Estimated value Estimated value (interpolated, extrapolated, or other estimation)
# '9' Missing value Missing value. Data parameter will record FillValue.
# ' ' FillValue Empty space in netcdf file. Empty space in netcdf file.

# We decided to remove no values here; bad values will be reject by other processing.
###-------------------------------------------------------------------------
if(applyImport){
  df=rio::import(dataFile)
  #write.csv(df[1:30000,],"CARNOT_reduit.csv",row.names = F)
  head(df)
  summary(df)
  nameOriginal=colnames(df)
  nameCol=colnames(df)
  nameCol=sapply(nameCol,FUN=function(x){out<-str_remove_all(x,"\\(")})
  nameCol=sapply(nameCol,FUN=function(x){out<-str_remove_all(x,"\\)")})
  nameCol=sapply(nameCol,FUN=function(x){out<-str_remove_all(x,"/")})
  nameCol=sapply(nameCol,FUN=function(x){out<-str_remove_all(x,"%")})

  colnames(df)=nameCol
  rm(nameCol)

  #QC EXTRACTION
  indQC=grep(pattern = "QC",x=colnames(df))
  QCmatrix=NULL
  tableQC=NULL
  if(length(indQC) == 1){
    QCmatrix <- Coriolis_QCmatrixExtraction(df,indQC)
    QCmatrix=as.data.frame(QCmatrix)
  
    #print the number of QC code
    tableQC=table(unlist(QCmatrix))
    print(100*tableQC/(nrow(QCmatrix)*ncol(QCmatrix)))
  
    #print list of more than 50% data with QC>1 
    res=sapply(QCmatrix,FUN=median,na.rm = T)
    print("maybe some troubles to analyze for these parameters: (no alignment; two many NA; ...)")
    print(res[res>1])
    
  } 

  #keep date/time/longitude/latitude if existing
  indDate=grep("date",colnames(df),ignore.case = T)
  indLongitude=grep("longitude",colnames(df),ignore.case = T)
  indLatitude=grep("latitude",colnames(df),ignore.case = T)
  date=df[,indDate]
  longitude=df[,indLongitude]
  latitude=df[,indLatitude]
  
  #keep index of numeric Parameter
  resNumeric=sapply(df,FUN=is.numeric)
  resMeta=sapply(df,FUN=function(x){length(na.omit(unique(x)))>1})
  indParam=which(resNumeric*resMeta==1)
  indParam=setdiff(indParam,c(indQC:ncol(df)))

  save(date,longitude,latitude,nameOriginal,indParam,indQC,df,tableQC,QCmatrix,file = file.path(resDir,"rawData.Rdata"))
 }

if(applyQCviewer){
  load(file.path(resDir,"rawData.Rdata"))
  
  # Raw print with QC - take times but interesting
  # for instance QC is not so relevant, see offset salinity QC = 1
  
  library("ggplot2")
  
  indQC=grep(pattern = "QC",x=colnames(df))
  fig=T
  if(fig){
    vdate=date
    nom=colnames(df)
    for(i in indParam){
      p=NULL;
      print(nom[i])
      matchQC=grep(pattern=nom[i],x=colnames(QCmatrix),ignore.case = T)
      print(matchQC)
      if(length(matchQC)>0){
        p=showMeasureCoriolis(vdate,df[[i]],nom[i],vQC=QCmatrix[[matchQC]],vColor=NULL);
        corName=str_replace(nom[i]," ", "_ ")
        corName=str_replace(nom[i],"-", "under")
        print(corName)
        ggsave(filename=paste(resFIG,"/",corName,".jpeg",sep=""),plot=p)
      }
    }
    rm(i); rm(p);
  }
}

if(!is.null(QCtoremove)){
  load(file.path(resDir,"rawData.Rdata"))
  indQC=grep(pattern = "QC",x=colnames(df))
  df=df[,1:(indQC-1)]
  
  for (code in QCtoremove){
    df[QCmatrix==code]=NA
  }
  
  save(df,QCmatrix,tableQC,longitude, indParam,latitude,date,nameOriginal,file=file.path(resDir,"cor1.Rdata"))
  
}else{  
  load(file.path(resDir,"rawData.Rdata"))
  save(df,QCmatrix,tableQC,longitude, indParam,latitude,date,nameOriginal,file=file.path(resDir,"cor1.Rdata"))
}


#-----------------------------
# 2- Sensors range check
#-----------------------------
if(applyMetrology){
  load(file.path(resDir,"cor1.Rdata"))
  QCmodified=QCmatrix
  dfc=df
  
  # decomment next line in order to write a specific sensorRange file 
  # df.sensor=data.frame(nom,minRange=NA,maxRange=NA)
  # write_csv2(df.sensor,file="SensorsRange.csv")
  df.sensor=rio::import(sensorsFile)
  nom=colnames(dfc)
  s1=summary(dfc)
  for(n in nom){
    print(n)
    ind=NULL
    ind=grep(pattern=gsub("^\\s+|\\s+$", "", n),x = df.sensor$name)
    if(length(ind)>0){
      minval=df.sensor$minRange[ind]
      maxval=df.sensor$maxRange[ind]
      index=NULL
      if(!is.na(minval)){
        print(minval)
        index=which(dfc[,n]<minval)
        dfc[index,n]=NA
        QCmodified[index,n]=4
      }
      if(!is.na(maxval)){
        print(maxval)
        index=which(dfc[,n]>maxval)
        dfc[index,n]=NA
        QCmodified[index,n]=4
      }
    }
  }
  summary(dfc)

  library(ggplot2)
  vdate=dfc$`DATE yyyy-mm-ddThh:mi:ssZ`
  for (i in 6:length(nom)){
    p=showMeasureCoriolis(vdate,dfc[[i]],nom[i]);
    corName=str_replace(nom[i]," ", "_ ")
    corName=str_replace(nom[i],"-", "under")
    print(corName)
    ggsave(filename=paste(resFIG,"/",corName,"_Range.jpeg",sep=""),plot=p)
  }
  save(df,dfc,QCmatrix,QCmodified,tableQC,longitude, indParam,latitude,date,nameOriginal,file=file.path(resDir,"cor2Range.Rdata"))
  
}

#-----------------------------
# 3- Outlier removing according discontinuity in boxplot
#-----------------------------
if(applyMetrology){
  load(file.path(resDir,"cor2Range.Rdata"))
  nom=colnames(dfc)
  for(i in indParam){
    signal=NULL;
    corName=str_replace(nom[i]," ", "_ ")
    corName=str_replace(nom[i],"-", "under")
    print(corName)
    jpeg(filename = paste(resFIG,"/",corName,"_outliers.jpeg",sep=""))
    signal=resetOutliers(dfc[,i],tolerance=0.5,quantileprobs = 0.85,fig = T)
    signal1=signal;signal1[is.na(signal1)]=='a'
    signal0=dfc[,i];signal0[is.na(signal0)]=='a'
    if(length(QCmodified)){QCmodified[which(signal1!=signal0),i]=4}
    dev.off()
    dfc[,i]=signal
  }

  library(ggplot2)
  vdate=dfc$`DATE yyyy-mm-ddThh:mi:ssZ`
  for (i in indParam){
    p=showMeasureCoriolis(vdate,dfc[[i]],nom[i]);
    corName=str_replace(nom[i]," ", "_ ")
    corName=str_replace(nom[i],"-", "under")
    print(corName)
    ggsave(filename=paste(resFIG,"/",corName,"_Discont.jpeg",sep=""),plot=p)
  }

  save(dfc,indParam,vdate,file = file.path(resDir,"corrected.Rdata",sep=""))
}


#---------------------------------------------------
# 4- Time Alignment 
#---------------------------------------------------
if(timeAlignment){
  load(file.path(resDir,"corrected.Rdata",sep=""))
  df=dfc[,indParam]
  df$date=strptime(dfc$`DATE yyyy-mm-ddThh:mi:ssZ`,format="%Y-%m-%d %H:%M:%S",tz="GMT")
  
  # 1 Hour
  print("1 hour sampling")
  df.align=timeAlignmentDataframe(df,sampling = 1,fsampling="hour",verbose=T)
  save(df.align,file=file.path(resDir,"alignedData1Hour.Rdata"))
  
  #print("2-hour sampling")
  # 2 Hours
  #df.align=timeAlignmentDataframe(df,sampling = 2,fsampling="hour",verbose=T)
  #save(df.align,file=file.path(resDir,"alignedData2Hour.Rdata"))
  
  #print("3-hour sampling")
  # 3 Hours
  #df.align=timeAlignmentDataframe(df,sampling = 3,fsampling="hour",verbose=T)
  #save(df.align,file=file.path(resDir,"alignedData3Hour.Rdata"))
  # 12 Hours
  #df.align=timeAlignmentDataframe(df,sampling = 12,fsampling="hour",verbose=T)
  #save(df.align,file=file.path(resDir,"alignedData12Hour.Rdata"))
  
  #20 minutes
  print("20-min sampling")
  df.align=timeAlignmentDataframe(df,sampling =20,fsampling="minute",verbose=T)
  save(df.align,file=file.path(resDir,"alignedData20Min.Rdata"))
}  
  

#test 
load(file.path(resDir,"alignedData1hour.Rdata"))
fluo=df.align$`FLU3 LEVEL1 FFU`
date=df.align$date
plot(date,fluo)