Data directory: Rdata (two dataframes of MAREL CARNOT with temporal sensor alignment)

Code: required function

Script: Coriolis Preprocessing and cEGIM script with data arrivals simulation.

Some Figures to show results: in red high sampling / gray low sampling during this period. 

Idea of the algorithm (samplingAction function in samplingStrategy.R). We add a threshold value for ffu that expert must be tuned according the site in order to keep bloom information (in particular: small blooms) 

![plot1](IdealCase1_states.png) 

![plot2](IdealCase_samplingResults.png)

Results on Marel Carnot: Threshold estimated by Q3-quantile, it could be higher to keep more energy. It is tested for 20min, 1hour, 2hour.

![plot2](SamplingResults.png)


