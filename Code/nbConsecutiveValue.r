#' nbConsecutiveValue finds (index, number of consecutive time) of a given value 
#' @author Emilie Poisson Caillault
#' @date revision 2018.03.2 
#' @param x a univariate time series
#' @param val the given value to search in the time series x
#' @return null if x does not contain val else ((index, number of consecutive time)
#' @examples
#' x=c(0,0,0,1,1,0,0,1,1,1,0);
#' res=nbConsecutiveValue(x,val=0)
#' print(res);
nbConsecutiveValue<-function(x, val=0){
  v=x
  decal=0;
  indVal=NULL
  N=NULL;
  #case constant vector of val
  if(sum(v==val)==length(v)) return(c(1,length(v)));
  #else evaluate each segment of val
  while(length(v)){
    w=sum(v==val);
    ind2=length(v);
    if(w>0){
      ind=min(which(v==val));
      v[1:ind]=val; 
      indVal=c(indVal,ind+decal);
      nw=sum(v!=val)
      if(nw>0){
        ind2=min(which(v!=val));
      }
      N=c(N,max(ind2-ind,1));
    }
    v=x[-c(1:(ind2+decal))];
    decal=decal+ind2;
  }
  out<-cbind(indVal,N);
}  


#' merge close segments obtained by nbConsecutiveValue according distance 
#' @author Emilie Poisson Caillault
#' @date revision 2018.03.2 
#' @param x matrix with the begin index and the size of each segment (a line= a segment) from nbConsecutiveValue
#' @param val the given value to merge close segments
#' @return null if x does not contain val else (index, number of consecutive time)
#' @examples
#' x=c(0,0,0,1,1,0,0,1,1,1,0);
#' res=nbConsecutiveValue(x,val=0)
#' res=mergeCloseSegments(res,3)
#' print(res);
mergeCloseSegments<-function(x, val=10){
  endSeg=x[,1]+x[,2]
  gap=x[-1,1]-endSeg[-length(endSeg)]
  ind=which(gap>val)
  
}